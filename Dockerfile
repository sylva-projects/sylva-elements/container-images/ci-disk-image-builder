FROM ubuntu:24.04
LABEL \
	maintainer="Sylva team" \
	repo="https://gitlab.com/sylva-projects/sylva-elements/container-images/ci-image"

ENV DEBIAN_FRONTEND noninteractive
ENV TARGETOS "linux"
ENV TARGETARCH "amd64"
# renovate: datasource=github-releases depName=fluxcd/flux2 VersionTemplate=v
ENV FLUX_VERSION "2.4.0"
# renovate: datasource=github-releases depName=sigstore/cosign VersionTemplate=v
ENV COSIGN_VERSION "2.4.3"
# renovate: datasource=github-releases depName=oras-project/oras VersionTemplate=v
ENV ORAS_VERSION "1.2.2"

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

# Configure sigstore attachment for skopeo
COPY registries.d/default.yaml /etc/containers/registries.d/default.yaml

# hadolint ignore=DL3013, DL3008, DL3015, SC2086
RUN apt-get update\
  && apt-get install curl wget python3-pip git git-lfs jq qemu-utils gdisk dosfstools kpartx rpm libdnf2-common dnf lvm2 sudo initramfs-tools-bin initramfs-tools-core initramfs-tools bzip2 util-linux squashfs-tools gnupg gnupg2 apt-transport-https inetutils-ping uuid-runtime xfsprogs zstd procps zstd zypper qemu-system qemu-system-x86 ovmf skopeo genisoimage guestfs-tools linux-image-generic debootstrap -y --no-install-recommends\
  && curl -O -L "https://github.com/sigstore/cosign/releases/download/v${COSIGN_VERSION}/cosign_${COSIGN_VERSION}_amd64.deb"\
  && dpkg -i cosign_${COSIGN_VERSION}_amd64.deb\
  && curl -L https://github.com/fluxcd/flux2/releases/download/v${FLUX_VERSION}/flux_${FLUX_VERSION}_${TARGETOS}_${TARGETARCH}.tar.gz | tar -xzO flux > /usr/local/bin/flux\
  && chmod +x /usr/local/bin/flux \
  && flux -v\
  && curl -LO "https://github.com/oras-project/oras/releases/download/v${ORAS_VERSION}/oras_${ORAS_VERSION}_${TARGETOS}_${TARGETARCH}.tar.gz" \
  && mkdir -p oras-install/ \
  && tar -zxf oras_${ORAS_VERSION}_${TARGETOS}_${TARGETARCH}.tar.gz -C oras-install/ \
  && mv oras-install/oras /usr/local/bin/ \
  && rm -rf oras_${ORAS_VERSION}_${TARGETOS}_${TARGETARCH}.tar.gz oras-install/ \
  && pip3 install --no-cache-dir diskimage-builder --break-system-packages\
  && sed -i 's/# obtain_device_list_from_udev = 0/obtain_device_list_from_udev = 0/g' /etc/lvm/lvm.conf\
  && sed -i 's/# external_device_info_source = "none"/external_device_info_source = "none"/g' /etc/lvm/lvm.conf\
  && sed -i 's/# udev_sync = 1/udev_sync = 0/g' /etc/lvm/lvm.conf\
  && sed -i 's/# udev_rules = 1/udev_rules = 0/g' /etc/lvm/lvm.conf\
  && touch /.dockerenv
